# [marketingtools.ru](https://marketingtools.ru) source codes

<br/>

### Run marketingtools.ru on localhost

    # vi /etc/systemd/system/marketingtools.ru.service

Insert code from marketingtools.ru.service

    # systemctl enable marketingtools.ru.service
    # systemctl start marketingtools.ru.service
    # systemctl status marketingtools.ru.service

http://localhost:4047
